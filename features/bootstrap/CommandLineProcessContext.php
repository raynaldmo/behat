<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

// make available getSession(), getPage() etc. method from Mink Library
use \Behat\MinkExtension\Context\RawMinkContext;

// require __DIR__.'/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

/**
 * Defines application features from the specific context.
 */
class CommandLineProcessContext extends RawMinkContext implements Context
{
    private $output;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * Called before each Scenario
     */
    public function __construct()
    {
        // These commands should be in BeforeScenario hook
        // mkdir('test');
        // chdir('test');
    }

    /**
     * @BeforeScenario
     */
    public function moveIntoTestDir()
    {
        if (!is_dir('test')) {
            mkdir('test');
        }
        chdir('test');
    }

    /**
     * @AfterScenario
     */
    public function moveOutOfTestDir()
    {
        chdir('..');
        if (is_dir('test')) {
            system('rm -fr '.realpath('test'));
        }
    }

    /**
     * @Given there is a file named :filename
     */
    public function thereIsAFileNamed($filename)
    {
        touch($filename);
    }

    /**
     * @When I run :command
     */
    public function iRun($command)
    {
        $this->output = shell_exec($command);
    }

    /**
     * @Then I should see :string in the output
     */
    public function iShouldSeeInTheOutput($string)
    {
        // if (strpos($this->output, $string) == false) {
        // throw new \Exception(sprintf('Expected "%s" in the output but got %s"', $string, $this->output));
        // }

        // Use phpunit assert functions instead of above
        assertContains(
            $string,
            $this->output,
            sprintf('Expected "%s" in the output but got %s"', $string, $this->output)
        );
    }
}
