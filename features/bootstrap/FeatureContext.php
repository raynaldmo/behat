<?php
use AppBundle\Entity\User;
use AppBundle\Entity\Product;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

// this make the Mink Library methods getSession(), getPage() etc. available
// for our use
use \Behat\MinkExtension\Context\RawMinkContext;

// this makes Symfony2 methods available for our use
use \Behat\Symfony2Extension\Context\KernelDictionary;
use \Doctrine\Common\DataFixtures\Purger\ORMPurger;


require __DIR__ . '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawMinkContext implements Context {

  // Got this trait by installing behat/symfony2-extension
  use KernelDictionary;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   *
   * Called before each Scenario
   */
  public function __construct() {
  }


  /**
   * @When I fill in the search box with :term
   */
  public function iFillInTheSearchBoxWith($term) {
    // search box doesn't have an id but does have a name attribute so we can
    // use that
    // name = searchTerm
    // find element
    $searchBox = $this->getSession()
      ->getPage()
      ->find('css', '[name="searchTerm"]');

    // abort if we don't find the element
    assertNotNull($searchBox, 'The search box was not found');

    // perform action: set value for element
    $searchBox->setValue($term);

  }

  /**
   * @When I press the search button
   */
  public function iPressTheSearchButton() {
    // find element
    $button = $this->getSession()
      ->getPage()
      ->find('css', '#search_submit');

    // abort if we don't find the element
    assertNotNull($button, 'The search button cannot be found');

    // perform action: press the search button
    $button->press();
  }

  /**
   * @Given there is an admin user :username with password :password
   */
  public function thereIsAnAdminUserWithPassword($username, $password) {
    // Insert user into database with $username and $password
    $user = new User();
    $user->setUsername($username);
    $user->setPlainPassword($password);
    $user->setRoles(['ROLE_ADMIN']);


    $em = $this->getContainer()->get('doctrine')->getManager();
    $em->persist($user);
    $em->flush();

    return $user;
  }

    /**
     * @Given there are :count products
     */
    public function thereAreProducts($count)
    {
        $em = $this->getEntityManager();

        for ($i=0; $i < $count; $i++) {
            $product = new Product();
            $product->setName('Product', $i);
            $product->setPrice(rand(10,1000));
            $product->setDescription('lorem');

            $em->persist($product);
        }
        $em->flush();
    }

    /**
     * @When I click :linkText
     */
    public function iClick($linkText)
    {
        $this->getSession()->getPage()->clickLink($linkText);
    }

    /**
     * @Then I should see :count products
     */
    public function iShouldSeeProducts($count)
    {
        // To "see "products" we need to find the page that has the products
        // table and check the table has correct number of rows.

        // Get /admin/products page
        $page = $this->getSession()->getPage();

        // Find table we're looking for
        $table = $page->find('css', 'table.table');

        assertNotNull($table, 'Could not find products table');
        assertCount(intval($count), $table->findAll('css', 'tbody tr'));
    }

    /**
     * @Given I am logged in as an admin
     */
    public function iAmLoggedInAsAnAdmin()
    {
        $this->thereIsAnAdminUserWithPassword('admin', 'admin');
        $this->visitPath('/app_dev.php/login');
        $page = $this->getSession()->getPage();
        $page->fillField('Username', 'admin');
        $page->fillField('Password', 'admin');
        $page->pressButton('Login');
    }


    /**
     * @return \Doctrine\ORM\EntityManager|object
     */
    private function getEntityManager() {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        return $em;
    }

    /**
   * @BeforeScenario
   */
  public function clearData() {
    // Put database into a "predictable" state before each scenario
    $em = $this->getContainer()->get('doctrine')->getManager();
    // $em->createQuery('DELETE FROM AppBundle:Product')->execute();
    // $em->createQuery('DELETE FROM AppBundle:User')->execute();

    // better way to clear database
    $purger = new ORMPurger($em);
    $purger->purge();

  }

}

