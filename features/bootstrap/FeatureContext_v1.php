<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

// make available getSession(), getPage() etc. method from Mink Library
use \Behat\MinkExtension\Context\RawMinkContext;

require __DIR__ . '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawMinkContext implements Context {

  private static $container;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   *
   * Called before each Scenario
   */
  public function __construct() {
  }

  /**
   * @BeforeSuite
   *
   * We bootstrap Symfony so that we get access to the service container and
   * database API (via doctrine)
   */
  public static function bootstrapSymfony() {
    require __DIR__ . '/../../app/autoload.php';
    require __DIR__ . '/../../app/AppKernel.php';

    $kernel = new AppKernel('test', TRUE);
    $kernel->boot();

    self::$container = $kernel->getContainer();

  }

  /**
   * @When I fill in the search box with :term
   */
  public function iFillInTheSearchBoxWith($term) {
    // search box doesn't have an id but does have a name attribute so we can
    // use that
    // name = searchTerm
    // find element
    $searchBox = $this->getSession()
      ->getPage()
      ->find('css', '[name="searchTerm"]');

    // abort if we don't find the element
    assertNotNull($searchBox, 'The search box was not found');

    // perform action: set value for element
    $searchBox->setValue($term);

  }

  /**
   * @When I press the search button
   */
  public function iPressTheSearchButton() {
    // find element
    $button = $this->getSession()
      ->getPage()
      ->find('css', '#search_submit');

    // abort if we don't find the element
    assertNotNull($button, 'The search button cannot be found');

    // perform action: press the search button
    $button->press();
  }

  /**
   * @Given there is an admin user :username with password :password
   */
  public function thereIsAnAdminUserWithPassword($username, $password) {
    // Insert user into database with $username and $password
    $user = new \AppBundle\Entity\User();
    $user->setUsername($username);
    $user->setPlainPassword($password);
    $user->setRoles(['ROLE_ADMIN']);


    $em = self::$container->get('doctrine')->getManager();
    $em->persist($user);
    $em->flush();

    return $user;
  }

  /**
   * @BeforeScenario
   */
  public function clearData() {
    // Put database into a "predictable" state before each scenario
    $em = self::$container->get('doctrine')->getManager();
    $em->createQuery('DELETE FROM AppBundle:Product')->execute();
    $em->createQuery('DELETE FROM AppBundle:User')->execute();
  }

}

