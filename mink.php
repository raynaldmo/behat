<?php
require __DIR__ . '/vendor/autoload.php';

// Example of using Mink Library to test web pages
// Mink has four important objects
// 1. Driver (Goutte)
// 2. Session - analogous to browser tab
// 3. Page - allows access to DOM
// 4. NodeElement

use Behat\Mink\Driver\GoutteDriver;
use Behat\Mink\Session;

$driver = new GoutteDriver();
$session = new Session($driver);

$session->visit('http://jurassicpark.wikia.com');

var_dump($session->getStatusCode(), $session->getCurrentUrl());

// DocumentElement
$page = $session->getPage();

// print out first 100 characters of page text elements
var_dump(substr($page->getText(), 0 , 100));

// NodeElement
$header = $page->find('css', '.WikiHeader .WikiNav h2');
var_dump($header->getText());

$nav = $page->find('css', '.subnav-2');
// var_dump($nav->getHtml());

$linkEl = $nav->find('css', 'li a');
var_dump($linkEl->getText());

// click on link
$linkEl->click();
var_dump($session->getCurrentUrl());

// Use name selector to find links, fields and buttons
// $link = $page->findLink('Novel');
// var_dump($link->getAttribute('href');

// $field = $page->findField('Description');
// $button = $page->findButton('Description');

